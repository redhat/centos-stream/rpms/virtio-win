#!/bin/sh

echo "Test that Windows XP\Windows Server 2003 are frozen and didn't change"

# Compare with the old package
URL_OLD=`head -n 1 package.cfg`
RPM_OLD=`tail -n 1 package.cfg`

echo "Old package URL " $URL_OLD
echo "Old rpm" $RPM_OLD

wget $URL_OLD

# Unpack previous version ISO
rpm2cpio $RPM_OLD | cpio -idmv

# Mount previous ISO
mkdir mnt_iso_old
mount ./usr/share/virtio-win/virtio-win.iso  mnt_iso_old -o loop

# Check unchanged files(winxp/win2k3/2k8)
echo "begin to check if the files for winxp/2003 are unchanged"
for i in `find mnt_iso -maxdepth 4 -mindepth 1 -type f | grep xp`;do diff $i ${i/mnt_iso/mnt_iso_old} >> freeze_results.txt; done
for i in `find mnt_iso -maxdepth 4 -mindepth 1 -type f | grep 2k3`;do diff $i ${i/mnt_iso/mnt_iso_old} >> freeze_results.txt; done

if [ -s freeze_results.txt ]
then
    echo "Failure: Files for Windows XP/2003 were changed"
    exit 1
else
    echo "Success: Files for Windows XP/2003 were not changed"
    exit 0
fi
